<!DOCTYPE html>
<html lang="en">
<form action="/templateppl2/updatedbmhs" method="post">
        <table class="table table-bordered" style="width:50%;margin: auto; margin-bottom: 20px">
        <?php foreach ($mahasiswa as $mhs) : ?>
            <tr>
                <th>NIM:</th>
                <td><input type="text" name="nim" class="nim" value="<?= $mhs->NIM ?>" disabled="disabled"></td>
            </tr>
            <tr>
                <th>NAMA:</th>
                <td><input type="text" name="nama" class="nama" value="<?= $mhs->NM_MHS ?>"></td>
            </tr>
            <tr>
                <th>TEMPAT TGL LAHIR:</th>
                <td><input type="text" name="tempat_lahir" class="tempat_lahir" value="<?= $mhs->TEL ?>"></td>
            </tr>
            <tr>
                <th>TANGGAL LAHIR:</th>
                <td><input type="text" name="tgl_lahir" class="tgl_lahir" value="<?= $mhs->TAL ?>"></td>
            </tr>
            <tr>
                <th>KELAS:</th>
                <td><input type="text" name="kelas" class="kelas" value="<?= $mhs->KELAS ?>"></td>
            </tr>
        <?php endforeach; ?>
    </table>
        <button type="submit" style="float: right" class="btn btn-primary">Simpan</button>
    </form>