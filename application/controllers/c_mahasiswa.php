<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_Mahasiswa extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("login"));
		}
		$this->load->helper("url");
		$this->load->model('m_mahasiswa');
	}
	public function index()
	{
		$this->load->library('table');

		$this->table->set_heading('Nomor', 'Name', 'NIM', 'UMUR', 'TANGGAL LAHIR', 'GAMBAR', 'View Detail');
		$query = $this->m_mahasiswa->querydataMHS();
		$i = 1;
		foreach ($query->result() as $row) {
			$baseURL = base_url("images/$row->image.jpg");
			$imagePath = "<img src=$baseURL width='100' height='150'>";
			$viewdetailpath = "<a href= viewdetailmahasiswa/$row->nim>View Detail</a>";
			$this->table->add_row($i, $row->nama, $row->nim, $row->umur, $row->lahir, $imagePath, $viewdetailpath);
			$i++;
		}

		$template = array(
			'table_open'            => '<table style="align: center" table border="1" cellpadding="4" cellspacing="0">',

			'thead_open'            => '<thead>',
			'thead_close'           => '</thead>',

			'heading_row_start'     => '<tr>',
			'heading_row_end'       => '</tr>',
			'heading_cell_start'    => '<th>',
			'heading_cell_end'      => '</th>',

			'tbody_open'            => '<tbody>',
			'tbody_close'           => '</tbody>',

			'row_start'             => '<tr>',
			'row_end'               => '</tr>',
			'cell_start'            => '<td>',
			'cell_end'              => '</td>',

			'row_alt_start'         => '<tr>',
			'row_alt_end'           => '</tr>',
			'cell_alt_start'        => '<td>',
			'cell_alt_end'          => '</td>',

			'table_close'           => '</table>'
		);

		$this->table->set_template($template);


		$data['View'] = 'view_table';
		$this->load->view('v_template', $data);
	}
	function tambah()
	{
		$data['View'] = 'V_Input_MHS';
		$this->load->view('v_template', $data);
	}

	function inputMHSDB()
	{
		$nama = $this->input->post('nama');
		$nim = $this->input->post('nim');
		$umur = $this->input->post('umur');
		$lahir = $this->input->post('lahir');

		$data = array(
			'nama' => $nama,
			'nim' => $nim,
			'umur' => $umur,
			'lahir' => $lahir
		);

		$this->m_mahasiswa->inputdataMHS($data);
		redirect('');
	}

	public function cari()
	{
		$data['View'] = 'v_search';
		$this->load->view('v_template', $data);
	}

	public function hasil()
	{
		$this->load->library('table');


		$query = $this->m_mahasiswa->cariMHS();
		$i = 1;
		if ($query->num_rows() > 0) {
			$this->table->set_heading('Nomor', 'Name', 'NIM', 'UMUR', 'TANGGAL LAHIR', 'GAMBAR', 'View Detail');
			foreach ($query->result() as $row) {
				$baseURL = base_url("images/$row->image.jpg");
				$imagePath = "<img src=$baseURL width='100' height='150'>";
				$viewdetailpath = "<a href= viewdetailmahasiswa/$row->nim>View Detail</a>";
				$this->table->add_row($i, $row->nama, $row->nim, $row->umur, $row->lahir, $imagePath, $viewdetailpath);
				$i++;
			}
	
			$template = array(
				'table_open'            => '<table style="align: center" table border="1" cellpadding="4" cellspacing="0">',
	
				'thead_open'            => '<thead>',
				'thead_close'           => '</thead>',
	
				'heading_row_start'     => '<tr>',
				'heading_row_end'       => '</tr>',
				'heading_cell_start'    => '<th>',
				'heading_cell_end'      => '</th>',
	
				'tbody_open'            => '<tbody>',
				'tbody_close'           => '</tbody>',
	
				'row_start'             => '<tr>',
				'row_end'               => '</tr>',
				'cell_start'            => '<td>',
				'cell_end'              => '</td>',
	
				'row_alt_start'         => '<tr>',
				'row_alt_end'           => '</tr>',
				'cell_alt_start'        => '<td>',
				'cell_alt_end'          => '</td>',
	
				'table_close'           => '</table>'
			);
	
			$this->table->set_template($template);
	
		} else {
			$this->table->set_heading("Tidak ada Data");
		}
		$data['View'] = 'view_table';
		$this->load->view('v_template', $data);
	}

	function detail($nim)
	{
		$this->load->library('table');
		$query = $this->m_mahasiswa->detailMHS($nim);

		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$baseURL = base_url("images/$row->image.jpg");
				$imagePath = "<img src=$baseURL width='100' height='150'>";
				$data['nama'] = $row->nama;
				$data['nim'] = $row->nim;
				$data['umur'] = $row->umur;
				$data['lahir'] = $row->lahir;
				$data['imagepath'] = $imagePath;
			}
		} else {
			$this->table->set_heading("Tidak ada Data");
		}
		$data['View'] = 'V_Detail';
		$this->load->view('v_template', $data);
	}

	function update($nim)
	{
		$this->load->library('table');
		$data['nama'] = "Uprit";
		$query = $this->m_mahasiswa->updateMHS($nim,$data);


		$data['View'] = 'V_Home';
		$this->load->view('v_template', $data);
	}


	
}
